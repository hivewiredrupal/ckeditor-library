﻿/**
* Drupal-Friendly oEmbed Plugin plugin
* Licensed under the MIT license
* jQuery Embed Plugin: http://code.google.com/p/jquery-oembed/ (MIT License)
* Plugin for: http://ckeditor.com/license (GPL/LGPL/MPL: http://ckeditor.com/license)
*/

(function () {
    CKEDITOR.plugins.add('drupal_oembed', {
        icons: 'drupal_oembed',
        hidpi: true,
        requires: 'widget,dialog',
        lang: ['de', 'en', 'fr', 'nl', 'pl', 'pt-br', 'ru', 'tr'],
        version: 1.00,
        init: function (editor) {
            // Load jquery?
            loadjQueryLibaries();
            CKEDITOR.tools.extend(CKEDITOR.editor.prototype, {
                oEmbed: function (url, maxWidth, maxHeight, responsiveResize) {
                    if (url.length < 1 || url.indexOf('http') < 0) {
                        alert(editor.lang.drupal_oembed.invalidUrl);
                        return false;
                    }
                    function embed() {
                        if (maxWidth == null || maxWidth == 'undefined') {
                            maxWidth = null;
                        }

                        if (maxHeight == null || maxHeight == 'undefined') {
                            maxHeight = null;
                        }

                        if (responsiveResize == null || responsiveResize == 'undefined') {
                            responsiveResize = false;
                        }

                        embedCode(url, editor, false, maxWidth, maxHeight, responsiveResize);
                    }

                    if (typeof(jQuery.fn.oembed) === 'undefined') {
                        CKEDITOR.scriptLoader.load(CKEDITOR.getUrl(CKEDITOR.plugins.getPath('drupal_oembed') + 'libs/jquery.oembed_all.min.js'), function() {
                            embed();
                        });
                    } else {
                        embed();
                    }

                    return true;
                }
            });


            editor.widgets.add('drupal_oembed', {
                mask: true,
                dialog: 'drupal_oembed',
                allowedContent: 'a(!oembed);a iframe[*]',
                template: '<a href="" class="oembed">' +
                        '</a>',
                upcast: function (element) {
                    return element.name == 'a' && element.hasClass("oembed");
                }
            });

            editor.ui.addButton('drupal_oembed', {
                label: editor.lang.drupal_oembed.button,
                command: 'drupal_oembed',
                toolbar: 'insert,10',
                icon: this.path + "icons/" + (CKEDITOR.env.hidpi ? "hidpi/" : "") + "oembed.png"
            });

            function isValidImageURL(url) {
              return jQuery("<img>", {
                  src: url,
                  error: function() { return false; },
                  load: function() { return true; }
              });
            }

            String.prototype.beginsWith = function(string) {
                return (this.indexOf(string) === 0);
            };

            function loadjQueryLibaries() {
                if (typeof(jQuery) === 'undefined') {
                    CKEDITOR.scriptLoader.load('http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', function() {
                        if (typeof(jQuery.fn.oembed) === 'undefined') {
                            CKEDITOR.scriptLoader.load(
                                CKEDITOR.getUrl(CKEDITOR.plugins.getPath('drupal_oembed') + 'libs/jquery.oembed_all.min.js')
                            );
                        }
                    });

                } else if (typeof(jQuery.fn.oembed) === 'undefined') {
                    CKEDITOR.scriptLoader.load(CKEDITOR.getUrl(CKEDITOR.plugins.getPath('drupal_oembed') + 'libs/jquery.oembed_all.min.js'));
                }
            }

            function embedCode(url, instance, closeDialog, maxWidth, maxHeight, responsiveResize, widget) {
                jQuery.noConflict();
                // Check if this is an image.
                var img = new Image();
                img.src = url;
                var imageWidth = img.width;
                jQuery('body').oembed(url, {
                    onEmbed: function(e) {
                        var elementAdded = false;
                        // We add our own href.
                        if (widget) {
                          jQuery(widget.element).attr('href', url);
                          if (typeof e.code === 'string') {
                              if (widget.element.$.firstChild) {
                                  widget.element.$.removeChild(widget.element.$.firstChild);
                              }
                              widget.element.appendHtml('<span class="oembed_url">' + url +'</span>' + e.code);
                              elementAdded = true;
                          } else if (typeof e.code[0].outerHTML === 'string') {
                              if (widget.element.$.firstChild) {
                                  widget.element.$.removeChild(widget.element.$.firstChild);
                              }
                              widget.element.appendHtml('<span class="oembed_url">' + url +'</span>' + e.code[0].outerHTML);
                              elementAdded = true;
                          } else {
                              alert(editor.lang.drupal_oembed.noEmbedCode);
                          }
                        }
                        if (elementAdded) {
                          if(CKEDITOR.dialog.getCurrent()) {
                            if (closeDialog) {
                                CKEDITOR.dialog.getCurrent().hide();
                            }
                          }
                        }
                    },
                    onError: function(externalUrl) {
                      if (externalUrl.indexOf("vimeo.com") > 0) {
                        // I think this is old policy, silence alert.
                        //alert(editor.lang.drupal_oembed.noVimeo);
                      } else {
                        // Let's check if this is just an image that was copied and pasted.
                        if(isValidImageURL(externalUrl)) {
                          if (widget) {
                            jQuery(widget.element).attr('href', url);
                            widget.element.appendHtml('<img class="oembed_image" src="' + externalUrl +'" />');
                            if(CKEDITOR.dialog.getCurrent()) {
                              if (closeDialog) {
                                CKEDITOR.dialog.getCurrent().hide();
                              }
                            }
                          }
                        } else {
                          alert(editor.lang.drupal_oembed.Error);
                        }
                      }
                    },
                    maxHeight: maxHeight,
                    maxWidth: maxWidth,
                    useResponsiveResize: true,
                    embedMethod: 'editor'
                });
            }

            CKEDITOR.dialog.add('drupal_oembed', function(editor) {
                return {
                    title: editor.lang.drupal_oembed.title,
                    minWidth: CKEDITOR.env.ie && CKEDITOR.env.quirks ? 568 : 550,
                    minHeight: 155,
                    onShow: function() {
                    },

                    onOk: function() {
                    },
                    contents: [{
                        label: editor.lang.common.generalTab,
                        id: 'general',
                        elements: [{
                                type: 'html',
                                id: 'oembedHeader',
                                html: '<div style="white-space:normal;width:500px;padding-bottom:10px">' + editor.lang.drupal_oembed.pasteUrl + '</div>'
                            }, {
                                type: 'text',
                                id: 'embedCode',
                                focus: function() {
                                    this.getElement().focus();
                                },
                                label: editor.lang.drupal_oembed.url,
                                title: editor.lang.drupal_oembed.pasteUrl,
                                setup: function(widget) {
                                    if (widget.data.oembed) {
                                        this.setValue(widget.data.oembed);
                                    }
                                },
                                commit: function(widget) {

                                    var dialog = CKEDITOR.dialog.getCurrent(),
                                        inputCode = dialog.getValueOf('general', 'embedCode').replace(/\s/g, ""),
                                        resizetype = 'responsive',
                                        maxWidth = 640,
                                        maxHeight = 480,
                                        responsiveResize = true,
                                        editorInstance = dialog.getParentEditor(),
                                        closeDialog = true;

                                    if (inputCode.length < 1 || inputCode.indexOf('http') < 0) {
                                        alert(editor.lang.drupal_oembed.invalidUrl);
                                        return false;
                                    }

                                    embedCode(inputCode, editorInstance, closeDialog, maxWidth, maxHeight, true, widget);

                                    widget.setData('oembed', inputCode);
                                }
                            }]
                    }]
                };
            });


            editor.on('instanceReady', function(evt) {
              // We would like to restore any embedded oembeds to wyswyg editor.
              var editorData = jQuery(this.getData());
              jQuery.each(editorData.find('a.oembed'), function (index, element) {
                var url = jQuery(this).attr('href');
                  // Reset our html to contain the URL only.
                  jQuery(this).html('<span class="oembed_url">' + url +'</span>');
                  jQuery(this).oembed(url, {
                    onEmbed: function(e) {
                      // Retrieve the embed code, and append to anchor.
                      if (typeof e.code === 'string') {
                        jQuery(this).append(e.code);
                      } else if (typeof e.code[0].outerHTML === 'string') {
                        jQuery(this).append(e.code[0].outerHTML);
                      }
                    },
                    onError: function (externalUrl) {
                      if (externalUrl.indexOf("vimeo.com") > 0) {
                        var vimeoEmbed = {};
                        jQuery.ajax({
                          url: 'http://vimeo.com/api/oembed.json?url=' + externalUrl + '&maxwidth=640',
                          async: false,
                          dataType: 'json',
                          success: function(data) {
                            vimeoEmbed = data.html;
                          }
                        });
                        jQuery(this).html('<span class="oembed_url">' + url +'</span>' + vimeoEmbed);
                      }
                      else{
                        if(isValidImageURL(externalUrl)) {
                          jQuery(this).html('<img class="oembed_image" src="' + externalUrl + '" />');
                        }
                      }
                    },
                    maxHeight: 480,
                    maxWidth: 640,
                    useResponsiveResize: true,
                    embedMethod: 'editor'
                  });
              });
              var newEditorData = jQuery('<div>');
              jQuery.each(editorData, function(index, item){
                newEditorData.append(item);
              });
              this.setData(newEditorData.html());
            }); // End of instanceReady.
        } // End of init.
    });

}
)();